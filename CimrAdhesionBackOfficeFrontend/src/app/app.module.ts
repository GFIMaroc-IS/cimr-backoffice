import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './core/components/main-layout/main-layout.component';
import { FooterComponent } from './core/components/footer/footer.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HeaderComponent } from './core/components/header/header.component';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './home/components/home/home.component';
import { RouterModule } from '@angular/router';
import { HtppInterceptorService } from './core/services/htpp-interceptor.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BasicAuthHtppInterceptorService } from './authentication/services/basic-auth-htpp-interceptor.service';
import { ServerErrorInterceptor } from './core/filters/server-error-interceptor';
import { TokenHtppInterceptorService } from './authentication/services/token-htpp-interceptor.service';
import { AuthGuardService } from './authentication/services/auth-guard.service';
import { RecaptchaModule, RECAPTCHA_BASE_URL } from 'ng-recaptcha';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    CoreModule,
    ReactiveFormsModule,
    RecaptchaModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthHtppInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true
    }
    ,
    {
      provide: RECAPTCHA_BASE_URL,
      useValue: 'https://recaptcha.net/recaptcha/api.js', // use recaptcha.net script source for some of our users
    }
    ,
    {
      provide: HTTP_INTERCEPTORS, useClass: TokenHtppInterceptorService, multi: true
    },
    {
      provide: AuthGuardService
    }
    // ,
    // {
    //   provide: RECAPTCHA_V3_SITE_KEY, 
    //   useValue: recaptcha.siteKey,
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
