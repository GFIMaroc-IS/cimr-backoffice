import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {FormsModule} from '@angular/forms';
import {AuthGuardService as AuthGuard} from './services/auth-guard.service';
import { RecaptchaModule } from 'ng-recaptcha';



@NgModule({
  declarations: [LoginComponent],
    imports: [
        CommonModule, AuthenticationRoutingModule, FormsModule, 
        RecaptchaModule
    ],
  providers:[AuthGuard]
})
export class AuthenticationModule { }


