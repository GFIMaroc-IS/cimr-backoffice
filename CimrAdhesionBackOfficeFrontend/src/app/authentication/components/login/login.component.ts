import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import Swal from "sweetalert2";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  log: String;
  showSpinner = false;
  pass: String;
  captchaOk: boolean;
  

  constructor(private authentication: AuthenticationService, 
    private route: Router) {
    
  }

  ngOnInit(): void {
    this.captchaOk = false;
    this.showSpinner = false;
  }

  login(event) {
    this.showSpinner = true;
    console.log(this.pass);
    this.authentication.authenticate(this.log, this.pass).subscribe(
      data => {
        console.log(data);
        this.showSpinner = false;
        if(data.errors != null && data.errors.length > 0){
          var message = data.errors[0]
          Swal.fire('Attention',message,'error')
        } else {
          this.route.navigate(['/listDemandesAdhesion']);
        }
        
        

      },
      error => {
        console.log(error)
        this.showSpinner = false;
        throw error;

      }

    );
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    this.captchaOk = captchaResponse != null;
  }
  

}
