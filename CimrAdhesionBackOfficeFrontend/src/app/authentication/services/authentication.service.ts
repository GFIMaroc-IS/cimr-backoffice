import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import {BaseApiService} from '../../core/services/base-api.service';
import { convertCompilerOptionsFromJson } from "typescript";

export class User {
  constructor(public status: string) {}
}
declare var $: any;
@Injectable({
  providedIn: "root"
})
export class AuthenticationService extends BaseApiService{
  constructor(private httpClient: HttpClient) {
    super(httpClient);
  }
    // Provide username and password for authentication, and once authentication is successful,
    //store JWT token in session
  authenticate(username, password) {
    return super.post("authenticate", { username, password })
      .pipe(
        map((userData:any) => {
          console.log(userData.errors)
          if(userData.errors != null && userData.errors.length > 0){
            return userData;
          } else {
            console.log(userData)
            sessionStorage.setItem("username", username);
            let tokenStr = "Bearer " + userData.token;
            sessionStorage.setItem("token", tokenStr);
            var data = {roles:{}};
            data.roles = userData.grantedAuthorityList;
            console.log(data.roles)
            sessionStorage.setItem("authorizations", JSON.stringify(data));
            return userData;
          }
          
        })
      );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem("username");
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem("username");
  }

  hasRole(role:string):boolean{
    let authos = sessionStorage.getItem("authorizations");
    let found = false;
    if(authos!==undefined){
      let roles = JSON.parse(authos).roles;
      if(roles!==undefined){
        roles.forEach(element => {
          if(element.authority==role){
              found = true;
          }
        });
      }
    }
    return found;
  }

  isValidateur() {
    if (this.hasRole("VALIDATEUR")) {
      return true;
    }
    return false;
  }

  isApprobateur() {
    if (this.hasRole("APPROBATEUR")) {
      console.log('true')
      return true;
    }
    return false;
  }
}
