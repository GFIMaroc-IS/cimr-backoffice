import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseApiService } from 'src/app/core/services/base-api.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit
{
  ngOnInit(): void {
    $(".question-answer").click( function () {
      $(".question-answer").removeClass("active");
      $(this).addClass("active");
      // code to get value here ...
    });
  }

  constructor(private route: ActivatedRoute,
    private router: Router,
    private baseApiService: BaseApiService) {
	
  }

  step1(){
    
    this.baseApiService.get('api/adhesion/init').subscribe(
      data =>{
        this.router.navigate(['/step1']);
      });
    
  }
  
}
