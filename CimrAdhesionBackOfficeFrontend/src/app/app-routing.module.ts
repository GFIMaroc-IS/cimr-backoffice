import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './core/components/main-layout/main-layout.component';
import {AuthGuardService as AuthGuard} from './authentication/services/auth-guard.service';
import { LoginLayoutComponent } from './core/components/login-layout/login-layout.component';

const routes: Routes = [{path:'',
                        component:LoginLayoutComponent,
                        children:[
                          {path:'',loadChildren:() => import(`./authentication/authentication.module`).then(m => m.AuthenticationModule)},
                          {path:'login',loadChildren:() => import(`./authentication/authentication.module`).then(m => m.AuthenticationModule)}    
                        ]},
                        {path:'',
                        component:MainLayoutComponent,
                        children:[
                          {path:'',loadChildren:() => import(`./demande-adhesion/demande-adhesion.module`).then(m => m.DemandeAdhesionModule),canActivate:[AuthGuard]},
                          {path:'listDemandesAdhesion',loadChildren:() => import(`./demande-adhesion/demande-adhesion.module`).then(m => m.DemandeAdhesionModule),canActivate:[AuthGuard]}       
                        ]}];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
