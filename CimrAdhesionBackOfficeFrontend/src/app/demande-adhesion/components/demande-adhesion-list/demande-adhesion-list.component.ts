import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { PrintService } from 'src/app/core/services/print.service';
import { AdhesionService } from 'src/app/core/services/service-adhesion.service';

@Component({
  selector: 'app-demande-adhesion-list',
  templateUrl: './demande-adhesion-list.component.html',
  styleUrls: ['./demande-adhesion-list.component.css']
})
export class DemandeAdhesionListComponent implements OnInit
{
  listDemandes : any;
  pages = new Array(0);
  page: number = 1;
  url = 'listDemandesAdhesion';
  ifu = '';
  dateDu = '';
  dateAu = '';
  statut = '';

  ngOnInit(): void {
    this.getListDemandes();
  }

  constructor(private route: ActivatedRoute, 
    private adhesionService: AdhesionService,
    private router: Router,
    private authService: AuthenticationService) {
      
  }

  getListDemandes() {
    var profil = '';
    var service = '';
    if(this.authService.isValidateur()){
      profil = 'VALIDATEUR';
    } else if(this.authService.isApprobateur()){
      profil = 'APPROBATEUR';
    } 
    service = this.url + '?profil=' + profil;
    if(this.ifu != ''){
      service += '&ifu=' + this.ifu;
    }
    if(this.dateDu != ''){
      service += '&dateDu=' + this.dateDu;
    }
    if(this.dateAu != ''){
      service += '&dateAu=' + this.dateAu;
    }
    if(this.statut != ''){
      service += '&statut=' + this.statut;
    }
    service += '&page=' + this.page;
    this.adhesionService.listDemandesAdhesion(service).subscribe(
      (res : any) => {
        console.log(res)
        this.listDemandes = res.listDtos;
        this.pages = new Array(res.pages);
        console.log(this.listDemandes)
        console.log(this.pages)
      },
      err => {
        throw err;
      });
  }

  reset(){
    this.ifu = '';
    this.statut = '';
    this.dateDu = '';
    this.dateAu = '';
    this.getListDemandes();
  }

  details(id : any){
    this.router.navigate(['detail', id]);
  }

  next(event: any) {
    console.log("========event==============" + event)
    event.preventDefault();
    if (this.page + 1 <= this.pages.length) {
      this.page = this.page + 1;
      this.getListDemandes();
    }

  }
  
  preced(event: any) {
    event.preventDefault();
    if (this.page - 1 > 0) {
      this.page = this.page - 1;
      this.getListDemandes();
    }

  }
  
  last(event: any) {
    event.preventDefault();
    this.page = this.pages.length;
    this.getListDemandes();
  }
  
  first(event: any) {
    event.preventDefault();
    this.page = 1;
    this.getListDemandes();
  }

  selectPage(value: any) {
    this.page = value;
    this.getListDemandes();
  }
  
}
