import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeAdhesionListComponent } from './demande-adhesion-list.component';

describe('DemandeAdhesionListComponent', () => {
  let component: DemandeAdhesionListComponent;
  let fixture: ComponentFixture<DemandeAdhesionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandeAdhesionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandeAdhesionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
