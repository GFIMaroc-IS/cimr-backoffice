import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeAdhesionDetailComponent } from './demande-adhesion-detail.component';

describe('DemandeAdhesionDetailComponent', () => {
  let component: DemandeAdhesionDetailComponent;
  let fixture: ComponentFixture<DemandeAdhesionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandeAdhesionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandeAdhesionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
