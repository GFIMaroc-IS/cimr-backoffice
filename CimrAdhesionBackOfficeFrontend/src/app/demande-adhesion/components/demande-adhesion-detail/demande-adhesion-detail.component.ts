import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PrintService } from 'src/app/core/services/print.service';
import { AdhesionService } from 'src/app/core/services/service-adhesion.service';
import { ReferentielService } from 'src/app/core/services/referentiel-api.service';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-demande-adhesion-detail',
  templateUrl: './demande-adhesion-detail.component.html',
  styleUrls: ['./demande-adhesion-detail.component.css']
})
export class DemandeAdhesionDetailComponent implements OnInit
{

  
  id : any = 0;
  listPieces : any;
  raisonSociale : any;
  motifRefusDemande : any;
  motifRejetDocument : any;

	numRC : any;
	adresse : any;
	formeJuridique : any;
	ice : any;
	ifu : any;
	cnss : any;
	numTaxe : any;
	nomMandataire : any;
	prenomMandataire : any;
	cinMandataire : any;
	fonctMandataire : any;
	telMandataire : any;
	nomGestionnaire : any;
	prenomGestionnaire : any;
	emailGestionnaire : any;
	fonctGestionnaire : any;
	telGestionnaire : any;
	dateEffetSouscription: any;
	effectif: any;
	titulaire: any;
	rib : any;
	modePaiementId : any;
	modePaiementLibelle : any;
	produitId : any;
	produitLibelle : any;
	delegataires : any;
	listCategoriesHabilitation : any;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    });
    if(this.id != 0){
      this.detailDemandeAdhesion();
    }
  }

  constructor(private adhesionService: AdhesionService,
    private route: ActivatedRoute,
    private router: Router) {
	
  }

  detailDemandeAdhesion() {
    this.adhesionService.detailDemandeAdhesion('detailDemandeAdhesion?id=' + this.id).subscribe(
      (res: any) => {
        console.log(res)
        if(res != null && res.demande != null){
          this.raisonSociale = res.demande.raisonSociale;
          this.dateEffetSouscription = res.demande.dateEffetSouscription;
        }
        if (res != null && res.adhesion != null) {
        	this.populateAdhesion(res.adhesion);
        }
        if (res != null && res.contrat != null) {
        	this.populateContrat(res.contrat);
        }
        if(res != null && res.pieces != null && res.pieces.length > 0){
          this.listPieces = res.pieces;
          console.log(this.listPieces)
        }
      },
      err => {
        throw err;
      })
  }

  validateDemande(){
    this.adhesionService.validateDemandeAdhesion('validateDemandeAdhesion?id=' + this.id).subscribe(
      (res: any) => {
        console.log(res)
        if(res != null){
          if(res.errors != null && res.errors.length > 0){
            var message = res.errors[0];
            Swal.fire('Erreur',message,'error');
          } else {
            Swal.fire('Succès','Demande d\'adhésion validée avec succès','success');
            this.router.navigate(['/listDemandesAdhesion']);
          }
        }
      },
      err => {
        throw err;
      })
  }

  rejectDemande(){
    
      $("#closeModel").click();
    
    this.adhesionService.rejectDemandeAdhesion('rejectDemandeAdhesion?id=' + this.id 
    + '&motif=' + this.motifRefusDemande).subscribe(
      (res: any) => {
        console.log(res)
        if(res != null && res == true){
          this.router.navigate(['/listDemandesAdhesion']);
        }
      },
      err => {
        throw err;
      })
  }

  telechargerPiece(id: any){

  }

  rejeterPiece(id: any){
    console.log(this.motifRejetDocument)
    console.log(id)
  }

  ApprouverPiece(id: any){

  }

  motifRejetPiece(id: any){

  }

  populateAdhesion(adhesion){
	      
	    this.numRC = adhesion.numRC ;
	    this.raisonSociale = adhesion.raisonSociale ;
	    this.adresse = adhesion.adresse;
	    this.formeJuridique = adhesion.formeJuridique;
	    this.ice = adhesion.ice;
	    this.ifu = adhesion.ifu;
	    this.cnss = adhesion.cnss;
	    this.numTaxe = adhesion.numTaxe;
	    this.nomMandataire = adhesion.nomMandataire;
	    this.prenomMandataire = adhesion.prenomMandataire;
	    this.cinMandataire = adhesion.cinMandataire;
	    this.fonctMandataire = adhesion.fonctMandataire;
	    this.telMandataire = adhesion.telMandataire;
	    this.nomGestionnaire = adhesion.nomGestionnaire;
	    this.prenomGestionnaire = adhesion.prenomGestionnaire;
	    this.emailGestionnaire = adhesion.emailGestionnaire ;
	    this.fonctGestionnaire = adhesion.fonctGestionnaire ;
	    this.telGestionnaire = adhesion.telGestionnaire ;
	  }

  populateContrat(contrat){
	    this.produitLibelle = contrat.produit.libelle ;
	    //this.dateEffetSouscription = contrat.dateEffetSouscription;
	    this.effectif = contrat.effectif;
	    this.delegataires = contrat.delegataires;
	  }
}
