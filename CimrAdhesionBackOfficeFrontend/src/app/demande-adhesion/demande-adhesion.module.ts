import { DemandeAdhesionRoutingModule } from './demande-adhesion-routing.module';
import { NgModule } from '@angular/core';
import { DemandeAdhesionListComponent } from './components/demande-adhesion-list/demande-adhesion-list.component';
import { CoreModule } from '../core/core.module';
import { DemandeAdhesionDetailComponent } from './components/demande-adhesion-detail/demande-adhesion-detail.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from '../core/components/menu/menu.component';
import { PaginationComponent } from '../core/components/pagination/pagination.component';

@NgModule({
  declarations: [DemandeAdhesionListComponent, DemandeAdhesionDetailComponent, MenuComponent, PaginationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    DemandeAdhesionRoutingModule
  ]
})
export class DemandeAdhesionModule { }