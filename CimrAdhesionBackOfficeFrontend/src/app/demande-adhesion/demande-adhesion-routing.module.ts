import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DemandeAdhesionListComponent } from './components/demande-adhesion-list/demande-adhesion-list.component';
import { DemandeAdhesionDetailComponent } from './components/demande-adhesion-detail/demande-adhesion-detail.component';

const routes: Routes = [{path:'',component:DemandeAdhesionListComponent},
                       {path:"detail/:id",component:DemandeAdhesionDetailComponent}];

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(routes),
            FormsModule,
            ReactiveFormsModule],
  exports: [RouterModule]
})
export class DemandeAdhesionRoutingModule { }