import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

declare var $: any;

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  constructor(private authentication:AuthenticationService,
    private route:Router) {

  }

  ngOnInit(): void {

    $( function(){
      $('.avatar').click( function () {
        $('.user-menu-dropdown').toggleClass('show');
      })
    });
  }

  deconnecter(){
    this.authentication.logOut();
    this.route.navigate(['/login']);
  }

}
