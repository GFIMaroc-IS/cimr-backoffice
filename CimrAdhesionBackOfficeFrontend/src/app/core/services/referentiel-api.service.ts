import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpParams, HttpHeaders} from '@angular/common/http';
import {BaseApiService} from './base-api.service';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReferentielService{
  
  baseUrl:string=window["baseUrl"]+'api/referentiel/';
  constructor(private http:HttpClient) {
  }

  listCategoriesHabilitation(action:string): Observable<HttpResponse<any>>{
    console.log("call "+action);
    return  this.http.get<any>(this.baseUrl+action);
  }
  listModesPaiement(action:string): Observable<HttpResponse<any>>{
    console.log("call "+action);
    return  this.http.get<any>(this.baseUrl+action);
  }
  listProduits(action:string): Observable<HttpResponse<any>>{
    console.log("call "+action);
    return  this.http.get<any>(this.baseUrl+action);
  }

  findModePaiementByCode(action:string): Observable<HttpResponse<any>>{
    console.log("call "+action);
    return  this.http.get<any>(this.baseUrl+action);
  }

  findProduitByCode(action:string): Observable<HttpResponse<any>>{
    console.log("call "+action);
    return  this.http.get<any>(this.baseUrl+action);
  }
  
}


