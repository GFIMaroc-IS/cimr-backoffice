import { Injectable } from '@angular/core';
import { HttpClient,HttpResponse } from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {
  baseUrl:string=window["baseUrl"];
   options = {
    responseType: 'json' as const,
  };
  constructor(private http: HttpClient) {

  }
  get(action:string): Observable<HttpResponse<any>> {
    console.log("call "+action);

    return this.http.get<any>(this.baseUrl+action);
  }
  getWithOptions(action:string,option:{headers,search}): Observable<HttpResponse<any>> {
    console.log("inside getWithOptions----call "+action);
    console.log("inside getWithOptions----options "+option);
    return this.http.get<any>(this.baseUrl+action,option);
  }
  post(action:string,body:any): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.post<any>(this.baseUrl+action,body);
  }
  put(action:string,body:any): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.put<any>(this.baseUrl+action,body);
  }

  remove(action:string): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.delete<any>(this.baseUrl+action);
  }


}
