import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse, HttpParams, HttpHeaders} from '@angular/common/http';
import {BaseApiService} from './base-api.service';
import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdhesionService{
  
  baseUrl:string=window["baseUrl"]+'bo/adhesion/';
  constructor(
    private http:HttpClient,
    private baseApiService: BaseApiService) {
  }
  
  listDemandesAdhesion(action:string): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.get<any>(this.baseUrl+action);
  }

  detailDemandeAdhesion(action:string): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.get<any>(this.baseUrl+action);
  }

  validateDemandeAdhesion(action:string): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.get<any>(this.baseUrl+action);
  }

  rejectDemandeAdhesion(action:string): Observable<HttpResponse<any>> {
    console.log("call "+action);
    return this.http.get<any>(this.baseUrl+action);
  }
  
}


