import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { TopbarComponent } from './components/topbar/topbar.component';
import { MenuComponent } from './components/menu/menu.component';
import { LoginLayoutComponent } from './components/login-layout/login-layout.component';
import { PaginationComponent } from './components/pagination/pagination.component';

@NgModule({
  declarations: [MainLayoutComponent, LoginLayoutComponent, HeaderComponent, 
    FooterComponent, TopbarComponent],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CoreModule { }